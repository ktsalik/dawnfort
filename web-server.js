var express = require('express');
const session = require('express-session');
var moment = require('moment');
var uniqid = require('uniqid');

var WebServer = function(database, engine) {
  this.instance = express();

  this.instance.use(express.static(__dirname + '/client'));
  this.instance.use('socket.io', express.static(__dirname + '/node_modules/socket.io-client'));

  this.instance.use(session({
    'secret': 'MasterakoS',
    resave: false,
    saveUninitialized: true,
  }));

  this.instance.use(express.json());

  this.instance.use(function(req, res, next) {
    next();
  });

  this.instance.get('/', function(req, res) {
    res.sendFile(__dirname + '/client/game.html');
  });

  this.instance.get('/status', async function (req, res) {
    if (req.session.user) {
      var userDetails = (await database.query(`SELECT id, username FROM users WHERE id = ${req.session.user}`))[0];
      var userCharacter = (await database.query(`SELECT * FROM characters WHERE user_id = ${userDetails.id}`))[0];
      userDetails.character = userCharacter;
      userDetails.grainPrice = engine.grainPrice;
      userDetails.private = req.session.private;
      res.json(userDetails);
    } else {
      res.json({
        status: 'unknown',
        message: 'not connected',
      });
    }
  });

  this.instance.get('/stats', async function (req, res) {
    if (req.session.user) {
      res.json((await database.query(`SELECT * FROM stats WHERE user_id = ${req.session.user}`))[0]);
    } else res.sendStatus(400);
  });

  this.instance.get('/train', async function(req, res) {
    var attribute = req.query.attribute;
    var points = parseInt(req.query.points);
    if (req.session.user && ['strength', 'agility', 'vitality', 'skill'].includes(attribute) && points >= 0) {
      var characterValues = (await database.query(`SELECT * FROM characters WHERE user_id = ${req.session.user}`))[0];
      if (characterValues._energy >= points) {
        var addedPoints = (points * 0.333) * Math.random();
        addedPoints = addedPoints.toFixed(4);
        var energyLeft = characterValues._energy - points;
        await database.query(`UPDATE characters SET ${attribute} = ${characterValues[attribute] + parseFloat(addedPoints)}, _energy = ${energyLeft}`);
        if (attribute == 'vitality') {
          await database.query(`UPDATE characters SET life = ${parseInt(100 + characterValues[attribute] + parseFloat(addedPoints))}`);
        }
        res.json({
          status: 'ok',
          message: `You successfully trained and gained ${addedPoints} of ${attribute}, you now have ${energyLeft} energy remaining.`,
        });
      } else {
        res.json({
          status: 'error',
          message: `You do not have enough energy to train this amount.`,
        });
      }
    }
  });

  this.instance.get('/user/:id/scavenge/:area', async function(req, res) {
    if (!req.session.user) return;
    res.json((await engine.character.scavenge(req.session.user, req.params.area)));
  });

  this.instance.get('/user/:id/land', async function(req, res) {
    if (!req.session.user) return;
    var arces = (await database.query(`SELECT * FROM arces WHERE user_id = ${req.session.user}`));
    res.json(arces);
  });

  this.instance.get('/user/:id/land/:arceId/sow', async function (req, res) {
    if (!req.session.user) return;
    var arce =  (await database.query(`SELECT * FROM arces WHERE id = ${req.params.arceId}`))[0];
    if (arce.state == 0) {
      await database.query(`UPDATE arces SET sow = '${moment().format('YYYY-MM-DD HH:mm:ss')}', state = 1 WHERE id = ${req.params.arceId}`);
      res.json({
        status: 'ok',
        message: `Sow for Arce #${arce.id} has now started`,
      });
      return;
    }
    res.sendStatus(400);
  });

  this.instance.get('/user/:id/land/:arceId/harvest', async function (req, res) {
    if (!req.session.user) return;
    var arce = (await database.query(`SELECT * FROM arces WHERE id = ${req.params.arceId}`))[0];
    if (arce.state == 2) {
      var character = (await database.query(`SELECT * FROM characters WHERE user_id = ${req.session.user}`))[0];
      await database.query(`UPDATE characters SET grain = ${character.grain + 1} WHERE id = ${character.id}`);
      await database.query(`UPDATE arces SET state = 0 WHERE id = ${req.params.arceId}`);
      res.json({
        status: 'ok',
        message: `Harvest of Arce #${arce.id} completed`,
      });
      return;
    }
    res.sendStatus(400);
  });

  this.instance.get('/user/:id/land/buy', async function(req, res) {
    if (!req.session.user) return;
    var character = (await database.query(`SELECT * FROM characters WHERE user_id = ${req.session.user}`))[0];
    var arces = (await database.query(`SELECT * FROM arces WHERE user_id = ${req.session.user}`));
    var arcesPrices = [1000, 5000, 10000, 25000, 50000, 100000];
    var arcePrice = arcesPrices[arces.length];
    arcePrice = arcePrice || 100000;
    if (character.vaccines >= arcePrice) {
      await database.query(`UPDATE characters SET vaccines = ${character.vaccines - arcePrice} WHERE user_id = ${req.session.user}`);
      await database.query(`INSERT INTO arces (user_id) VALUES (${req.session.user})`);
      res.json({
        status: 'ok',
        message: 'You bought an arce of land',
      });
    } else {
      res.json({
        status: 'error',
        message: 'Not enough resources to buy an arce',
      });
    }
  });

  this.instance.get('/sell-grain', async function(req, res) {
    if (!req.session.user) return;
    if (!parseInt(req.query.grain)) return;
    var character = (await database.query(`SELECT * FROM characters WHERE user_id = ${req.session.user}`))[0];
    if (character.grain >= parseInt(req.query.grain)) {
      var vaccinesEarned = engine.grainPrice * parseInt(req.query.grain);
      await database.query(`UPDATE characters SET vaccines = ${character.vaccines + vaccinesEarned}, grain = ${character.grain - req.query.grain} WHERE id = ${character.id}`)[0];
      res.json({
        status: 'ok',
        message: `You successfully sold ${req.query.grain} tonnes of grain. You earned ${vaccinesEarned} vaccines`,
      });
    } else {
      res.json({
        status: 'error',
        message: `You do not own enough grain`,
      });
    }
  });

  this.instance.get('/user/:id/levelup', async function(req, res) {
    if (!req.session.user) return;
    
    var getLevelExp = function (level) {
      var exp = (level * 111);
      exp += parseInt(level / 10) * 333;
      exp += parseInt(level / 50) * 9999;
      exp += parseInt(level / 100) * 999999;
      return exp;
    };

    var character = (await database.query(`SELECT * FROM characters WHERE user_id = ${req.session.user}`))[0];
    var level = character.level;
    var exp = character.experience;
    while (true) {
      if (exp >= getLevelExp(level)) {
        exp -= getLevelExp(level);
        level++;
      } else {
        break;
      }
    }
    await database.query(`UPDATE characters SET level = ${level}, experience = ${exp} WHERE id = ${character.id}`);
    res.json({
      status: 'ok',
    });
  });

  this.instance.post('/login', async function(req, res) {
    if (req.body.account) {
      var user = (await database.query(`
        SELECT id, username 
        FROM users 
        WHERE (username = ? OR email = ?) AND password = ?
      `, [
        req.body.account,
        req.body.account,
        req.body.password,
      ]))[0];
      if (user) {
        req.session.user = user.id;
        req.session.private = uniqid();
        engine.characters.find(c => c.user_id == user.id).private = req.session.private;
        res.json({
          status: 'ok',
          message: 'logged in',
          data: user,
        });
        return;
      }
    }
    res.json({
      status: 'error',
      message: 'Invalid credentials',
    });
  });

  this.instance.get('/logout', function(req, res) {
    req.session.destroy();
    res.redirect('/');
  });

  this.instance.post('/register', async function (req, res) {
    var body = req.body;
    var valid = body.username && body.username.length >= 3 &&
      body.email && body.email.length >= 6 &&
      body.password && body.password.length >= 4;

    if (!valid) {
      res.json({
        status: 'error',
        message: 'Entered invalid details. All fields are required.',
      });
    } else {
      var alreadyUser = (await database.query(`
        SELECT id, username, password
        FROM users
        WHERE username = ? OR email = ?
      `, [body.username, body.email]))[0];

      if (alreadyUser) {
        if (body.password != alreadyUser.password) {
          res.json({
            status: 'error',
            message: 'Username or Email already exist',
          });
          return;
        } else {
          req.session.user = alreadyUser.id;
          req.session.private = uniqid();
          engine.characters.find(c => c.user_id == alreadyUser.id).private = req.session.private;
          delete alreadyUser.password;
          res.json({
            status: 'ok',
            message: 'logged in',
            data: alreadyUser,
          });
          return;
        }
      }

      await database.query(`
        INSERT INTO users (username, email, password)
        VALUES (?, ?, ?)
      `, [
        body.username,
        body.email,
        body.password,
      ]);
      var userId = (await database.query(`SELECT id FROM users WHERE email = '${body.email}'`))[0].id;
      
      req.session.user = userId;
      req.session.private = uniqid();
      await engine.character.create(userId);
      engine.characters.find(c => c.user_id == userId).private = req.session.private;

      res.json({
        status: 'ok',
        message: 'joined the fight',
      });
    }
  });
};

WebServer.prototype.start = function(port) {
  this.instance.listen(port || 1337);
};

module.exports = WebServer;