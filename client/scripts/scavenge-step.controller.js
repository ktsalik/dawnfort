angular
  .module('DawnFortApp')
  .controller('ScavengeStepController', ['$scope', '$rootScope', '$routeParams', '$http', function ($scope, $rootScope, $routeParams, $http) {
    var vm = this;

    vm.area = $routeParams.area;

    vm.view = {
      areas: [
        {
          name: 'Restaurant',
          text: `Before the Rising, Rita's Diner was a hole in the wall beloved by everyone from gardeners cutting hedges to millionaires
  managing hedges. Now broken glass litters the ground, and the vinyl booths are shredded and filthy. It's been well
  picked over, but you've been told the parking lot still has a few landscaping trucks and trailers that haven't been
  broken into yet. Maybe you’ll be able to find some Bolt Cutters or Food?`,
          points: 1,
        },
        {
          name: 'Warehouse',
          text: `The industrial district of the city was hit the hardest when the bombs dropped. However, there's one that stands relatively unscathed. The windows are small to squeeze through, but through the broken, jagged glass, you can see the glint of gold. Maybe a pocket watch or a Compass? The only way in seems to be a massive door - that is also padlocked tight. You'll need Bolt Cutters to get the door open.`,
          points: 2,
        },
        {
          name: 'Shopping Mall',
          text: `The Shopping Mall was dying anyway due to the rise of Primo shipping, but the appearance of bitchy blonde zombies sealed its fate. Now it's abandoned and quiet, and ripe for scavenging. The only problem? The building is massive, and the mall map is impossible to read since it's covered in dried gore. A fellow survivor scratched some directions on the floor, but you'll need a Compass to figure out if you're going the right way.`,
          points: 3,
        },
        {
          name: 'College',
          text: `The local campus didn't stand a chance of surviving - not when the zombies made their first appearance during Alpha Sigma Phi's Annual Night of the Living Dead Costume Party. The bright side is that the fence surrounding Greek Row that kept the losers out is now keeping all the zombies in - and you've heard rumors that the frat house at the end of the row was secretly really into prepping. Their attic is filled with Gas Masks and vaccines. You just need to get some kind of rope - maybe a Paracord? - to help you over the fence and up the side of the house.`,
          points: 4,
        },
        {
          name: 'Arcade',
          points: 5,
        },
        {
          name: 'Cinema',
          points: 6,
        },
        {
          name: 'Hotel',
          points: 7,
        },
        {
          name: 'Bank',
          points: 8,
        },
        {
          name: 'Prison',
          points: 9,
        },
      ]
    };

    $rootScope.$watch('user', function(val) {
      vm.user = val;
    });

    vm.scavenge = function() {
      $http.get('user/' + vm.user.id + '/scavenge/' + vm.area).then(function(response) {
        $rootScope.updateStatus();
        vm.view.successMessage = vm.view.failMessage = vm.view.infoMessage = '';
        if (response.data.status == 'ok') {
          vm.view.successMessage = response.data.message;
        } else if (response.data.status == 'info') {
          vm.view.infoMessage = response.data.message;
        } else if (response.data.status == 'error') {
          vm.view.failMessage = response.data.message;
        }
      });
    };
  }]);