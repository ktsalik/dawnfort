angular
  .module('DawnFortApp')
  .controller('RaidController', ['$scope', '$location', '$route', '$rootScope', '$interval', '$timeout', function ($scope, $location, $route, $rootScope, $interval, $timeout) {
    var vm = this;

    vm.loadingView = true;

    $rootScope.$watch('user', function(val) {
      vm.user = val;
    });

    vm.createRaid = function() {
      $rootScope.ws.send('command', {
        type: 'create-raid',
      });
      vm.loadingView = true;
      $timeout(function() {
        vm.loadingView = false;
      }, 1111);
    };

    vm.joinRaid = function(raidId) {
      $rootScope.ws.send('command', {
        type: 'join-raid',
        id: raidId,
      });
    };

    vm.leaveRaid = function() {
      $rootScope.ws.send('command', {
        type: 'leave-raid',
      });
    };

    vm.raid = null;

    vm.live = {};

    function onPong(data) {
      vm.loadingView = false;
      data = data;
      vm.raids = data.raids;
      if (data.raid) {
        vm.raid = data.raid;
        var logs = [];
        var counts = [];
        var lastLog = null;
        for (var i = 0; vm.raid.logs && i < vm.raid.logs.length; i++) {
          if (lastLog != vm.raid.logs[i]) {
            logs.push(vm.raid.logs[i]);
            lastLog = vm.raid.logs[i];
            counts.push(1);
          } else {
            counts[counts.length - 1]++;
          }
        }
        vm.raid.logs = logs;
        vm.raid.logCounts = counts;
        vm.live.hp = data.hp;
        vm.live.players = data.players;
      } else {
        vm.raid = null;
      }
    }
    $rootScope.ws.socket.on('_pong', onPong);
    $rootScope.ws.send('_ping', { query: ['raid'], });

    var raidStatusInterval = $interval(function() {
      $rootScope.ws.send('_ping', {
        query: ['raid'],
      });
    }, 555);

    $scope.$on('$destroy', function() {
      $interval.cancel(raidStatusInterval);
      $rootScope.ws.socket.off('_pong', onPong);
    });

    vm.actionState = 'ready';

    async function actionDelay() {
      vm.actionState = 5;
      await new Promise(function (resolve) { $timeout(resolve, 1000); });
      vm.actionState = 4;
      await new Promise(function (resolve) { $timeout(resolve, 1000); });
      vm.actionState = 3;
      await new Promise(function (resolve) { $timeout(resolve, 1000); });
      vm.actionState = 2;
      await new Promise(function (resolve) { $timeout(resolve, 1000); });
      vm.actionState = 1;
      await new Promise(function (resolve) { $timeout(resolve, 1000); });
      vm.actionState = 'ready';
    }

    vm.attack = async function() {
      if (vm.actionState != 'ready') return;
      $rootScope.ws.send('command', { type: 'attack', });
      // actionDelay();
    };

    vm.revive = async function() {
      if (vm.actionState != 'ready') return;
      $rootScope.ws.send('command', { type: 'revive', });
      // actionDelay();
    };
  }]);