angular
  .module('DawnFortApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/index.html',
        controller: 'IndexController',
        controllerAs: 'vm',
        resolve: {
          user: ['$location', '$rootScope', '$http', function($location, $rootScope, $http) {
            return $http.get('status').then(function (response) {
              if (response.data.id) {
                $rootScope.user = response.data;
                if ($location.path() == '/') {
                  $location.path('safehouse');
                }
              } else {
                // $location.path('/');
              }
            });
          }]
        },
      })
      .when('/safehouse', {
        templateUrl: 'views/safehouse.html',
        controller: 'SafehouseController',
        controllerAs: 'vm',
      })
      .when('/scavenge', {
        templateUrl: 'views/scavenge.html',
        controller: 'ScavengeController',
        controllerAs: 'vm',
      })
      .when('/scavenge/:area', {
        templateUrl: 'views/scavenge-step.html',
        controller: 'ScavengeStepController',
        controllerAs: 'vm',
      })
      .when('/land', {
        templateUrl: 'views/land.html',
        controller: 'LandController',
        controllerAs: 'vm',
      })
      .when('/grain', {
        templateUrl: 'views/grain.html',
        controller: 'GrainController',
        controllerAs: 'vm',
      })
      .when('/raid', {
        templateUrl: 'views/raid.html',
        controller: 'RaidController',
        controllerAs: 'vm',
      })
      .when('/city', {
        templateUrl: 'views/city.html',
        controller: 'CityController',
        controllerAs: 'vm',
      })
      .when('/user', {
        templateUrl: 'views/user.html',
      })
    });