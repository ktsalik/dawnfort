angular
  .module('DawnFortApp')
  .directive('bar', [function() {
    return {
      restrict: 'C',
      scope: {
        value: '=',
        max: '=',
      },
      link: function ($scope, element, attrs) {
        $(element).append($(`<div class="fill"></div>`));

        $scope.$watch('value', function(val) {
          $(element).find('.fill').css('width', parseInt((val * 100) / $scope.max) + '%');
        });

        $scope.$watch('max', function (val) {
          
        });
      },
    };
  }]);