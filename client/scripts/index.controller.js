angular
  .module('DawnFortApp')
  .controller('IndexController', ['$scope', '$location', '$route', function($scope, $location, $route) {
    var vm = this;

    $('[ng-view]').css('padding', 0);
    $(function () {
      $('#login input').on('keypress', function(e) {
        if (e.keyCode == 13) {
          $('[btn-login]').click();
        }
      });

      $('[btn-login]').on('click', function() {
        $.ajax({
          url: 'login',
          method: 'POST',
          contentType: 'application/json',
          data: JSON.stringify({
            account: $('#login input').eq(0).val(),
            password: $('#login input').eq(1).val(),
          }),
          dataType: 'json',
          success: function (response) {
            $('#login .alert.danger').remove();
            if (response.status == 'error') {
              $('#login').prepend($(`<div class="alert danger">${response.message}</div>`));
            } else if (response.status == 'ok') {
              $scope.$apply(function () {
                // $route.reload();
                location.reload();
              });
            }
          },
          error: function () {
            $('#login .alert.danger').remove();
            // console.error('foo');
          },
          complete: function () {

          },
        });
      });

      $('[btn-register-step]').on('click', function () {
        $('#login').hide();
        $('#register').fadeIn(function () {
          $('#register input').eq(0).focus();
        });
      });

      $('[btn-register-back]').on('click', function () {
        $('#login').show();
        $('#register').hide();
      });

      $('[btn-register]').on('click', function () {
        $.ajax({
          url: 'register',
          method: 'POST',
          contentType: 'application/json',
          data: JSON.stringify({
            username: $('#register input').eq(0).val(),
            email: $('#register input').eq(1).val(),
            password: $('#register input').eq(2).val(),
          }),
          dataType: 'json',
          success: function (response) {
            $('#register .alert.danger').remove();
            if (response.status == 'error') {
              $('#register').prepend($(`<div class="alert danger">${response.message}</div>`));
            } else if (response.status == 'ok') {
              $scope.$apply(function() {
                location.reload();
              });
            }
          },
          error: function () {
            $('#register .alert.danger').remove();
            // console.error('foo');
          },
          complete: function () {

          },
        });
      });
    });
  }]);