angular
  .module('DawnFortApp')
  .controller('AppController', ['$scope', '$rootScope', '$interval', '$http', '$location', function($scope, $rootScope, $interval, $http, $location) {
    var vm = this;

    $scope.isMobile = $rootScope.isMobile;

    $rootScope.$watch('user', function(val) {
      vm.user = val;
    });

    $rootScope.updateStatus = function() {
      $http.get('status').then(function (response) {
        $rootScope.user = response.data;
      });
    };

    $interval(function() {
      $rootScope.updateStatus();
    }, 5000);

    vm.goto = function (path) {
      $location.path(path);
    };

    var socket = io();

    $rootScope.ws = {
      socket: socket,
      send: function(type, data) {
        data = data || {};
        data.private = vm.user.private;
        socket.emit(type, (data));
      },
    };
  }]);