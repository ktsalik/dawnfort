angular
  .module('DawnFortApp')
  .controller('LandController', ['$scope', '$rootScope', '$http', '$interval', '$timeout', '$filter', function($scope, $rootScope, $http, $interval, $timeout, $filter) {
    var vm = this;

    vm.view = {
      arcesPrices: [1000, 5000, 10000, 25000, 50000, 100000],
    };

    vm.isMobile = $scope.$parent.isMobile;

    $rootScope.$watch('user', function(val, oldVal) {
      var toUpdateLand = !vm.user;
      vm.user = val;
      if (toUpdateLand) {
        updateLand();
      }
    });

    var updateLand = function() {
      if (!vm.user) return;
      $http.get('user/' + vm.user.id + '/land').then(function (response) {
        vm.arces = response.data;

        vm.buyArcePrice = vm.view.arcesPrices[vm.arces.length];
        vm.buyArcePrice = vm.buyArcePrice || 100000;
        vm.buyArcePrice = $filter('commafy')(vm.buyArcePrice);

        $timeout(function () { // after render
          vm.arces.forEach(function (arce) {
            if (arce.state == 1) {
              var sow = moment(arce.sow);
              var duration = moment.duration(moment().diff(sow));
              arce.sowTimeLeft = parseInt(60 - duration.asMinutes());
              $('.arce[data-arce-id="' + arce.id + '"] .progress').css('width', ((60 - arce.sowTimeLeft) * 100) / 60 + '%');
            } else if (arce.state == 2) {
              $('.arce[data-arce-id="' + arce.id + '"] .progress').css('width', '100%');
            }
          });
        });
      });
    };
    
    var updateLandInterval = $interval(updateLand, 5000);

    $scope.$on('$destroy', function () {
      $interval.cancel(updateLandInterval);
    });

    vm.buyArce = function() {
      $http.get('user/' + vm.user.id + '/land/buy').then(function(response) {
        vm.view.successMessage = vm.view.failMessage = '';
        if (response.data.status == 'ok') {
          vm.view.successMessage = response.data.message;
          $rootScope.updateStatus();
          updateLand();
        } else if (response.data.status == 'error') {
          vm.view.failMessage = response.data.message;
        }
      });
    };

    vm.sowArce = function(arceId) {
      $http.get('user/' + vm.user.id + '/land/' + arceId + '/sow').then(function(response) {
        vm.view.successMessage = vm.view.failMessage = '';
        if (response.data.status == 'ok') {
          vm.view.successMessage = response.data.message;
          updateLand();
        }
      });
    };

    vm.harvestArce = function(arceId) {
      $http.get('user/' + vm.user.id + '/land/' + arceId + '/harvest').then(function(response) {
        vm.view.successMessage = vm.view.failMessage = '';
        if (response.data.status == 'ok') {
          vm.view.successMessage = response.data.message;
          updateLand();
        }
      });
    };

    vm.sowAllArces = function() {
      vm.arces.forEach(function(arce) {
        vm.sowArce(arce.id);
      });
    };

    vm.harvestAllArces = function () {
      vm.arces.forEach(function (arce) {
        vm.harvestArce(arce.id);
      });
    };
  }]);