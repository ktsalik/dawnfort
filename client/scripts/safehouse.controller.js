angular
  .module('DawnFortApp')
  .controller('SafehouseController', ['$scope', '$rootScope', '$http', '$interval', '$timeout', function($scope, $rootScope, $http, $interval, $timeout) {
    var vm = this;

    vm.view = {
      selectedTab: 'stats',
      stats: {
        pointsToSpend: {

        },
      },
    };
    
    vm.isMobile = $scope.$parent.isMobile;

    var getStats = function() {
      $http.get('stats').then(function (response) {
        vm.stats = response.data;
      });
    };

    getStats();
    var updateStatsInterval = $interval(function() {
      getStats();
    }, 5000);

    $scope.$on('$destroy', function() {
      $interval.cancel(updateStatsInterval);
    });

    $rootScope.$watch('user', function(val, oldVal) {
      if (!val) return;
      vm.user = val;
      $('.life-bar .life').css('width', parseInt((val.character.hp * 100) / val.character.life) + '%');
      ['strength', 'agility', 'vitality', 'skill'].forEach(function(attribute) {
        if (oldVal && oldVal.character[attribute] != val.character[attribute]) {
          var o = { _attribute: oldVal.character[attribute] * 10000, };
          anime({
            targets: o,
            duration: 333,
            easing: 'linear',
            _attribute: val.character[attribute] * 10000,
            update: function() {
              $scope.$apply(function() {
                vm.user.character['_' + attribute] = o._attribute / 10000;
              });
            },
          });
        } else {
          vm.user.character['_' + attribute] = val.character[attribute];
        }
      });
    });

    vm.addPoints = function(attribute) {
      if (parseInt(vm.view.stats.pointsToSpend[attribute])) {
        $http.get(`train?attribute=${attribute}&points=${vm.view.stats.pointsToSpend[attribute]}`).then(function (response) {
          $rootScope.updateStatus();
          if (response.data.status == 'ok') {
            vm.view.trainMessageSuccess = vm.view.trainMessageFail = '';
            vm.view.trainMessageSuccess = response.data.message;
          } else if (response.data.status == 'error') {
            vm.view.trainMessageSuccess = vm.view.trainMessageFail = '';
            vm.view.trainMessageFail = response.data.message;
          }
          vm.view.stats.pointsToSpend[attribute] = 0;
        });
      }
    };
  }]);