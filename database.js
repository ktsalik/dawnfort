var mysql = require('mysql');

var Database = function() {
  this.connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'password',
    database: 'dawnfort'
    // host: 'remotemysql.com',
    // user: '8zEd1xTB2K',
    // password: '6LxjR3zhqO',
    // database: '8zEd1xTB2K'
  });

  // connection.end();
};

Database.prototype.connect = function() {
  var _this = this;
  return new Promise(function(resolve, reject) {
    _this.connection.connect(function(error) {
      if (error) reject(error);
      else resolve();
    });
  })
};

Database.prototype.query = function(query, data) {
  var _this = this;
  return new Promise(function(resolve, reject) {
    _this.connection.query(query, data, function (error, results, fields) {
      if (error) reject(error);
      else resolve(results);
      // if (error) throw error;
      // console.log('The solution is: ', results[0].solution);
    });
  });
};

module.exports = Database;