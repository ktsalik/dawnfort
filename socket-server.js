var SocketServer = function(app, engine) {
  var http = require('http').Server(app);
  var io = require('socket.io')(http);

  var _this = this;
  this.users = [];

  io.on('connection', function (socket) {
    var user = {
      id: socket.id,
      socket: socket,
    };
    _this.users.push(user);
    user.socket.on('disconnect', function () {
      _this.users.splice(_this.users.findIndex(u => u.socket.id == user.socket.id), 1);
    });

    user.socket.on('command', async function(data) {
      data = data;

      switch (data.type) {
        case 'create-raid':
          var character = engine.characters.find(c => c.private == data.private);
          if (character) {
            engine.raidEngine.createRaid(character.id);
          }
          break;
        case 'join-raid':
          var character = engine.characters.find(c => c.private == data.private);
          if (character) {
            engine.raidEngine.joinRaid(data.id, character.id);
          }
          break;
        case 'leave-raid':
          var character = engine.characters.find(c => c.private == data.private);
          if (character) {
            engine.raidEngine.leaveRaid(character.id);
          }
          break;
        case 'attack':
          var character = engine.characters.find(c => c.private == data.private);
          if (character) {
            if (character.hp === 0) return;
            var raid = engine.raidEngine.raids.find(r => r.players.includes(character.id));
            if (raid.targets.length) {
              var target = raid.targets[0];
              var characterAd = character.strength * 0.7;
              var damage = parseInt(characterAd);
              if (target.stats.hp - damage > 0) {
                raid.targets[0].stats.hp -= damage;
                engine.raidEngine.log(raid.id, `*${character.name}* [attacks] -${raid.targets[0].name}- doing ${damage} damage.`);
              } else {
                engine.raidEngine.log(raid.id, `*${character.name}* [attacks] -${raid.targets[0].name}- doing ${damage} damage.`);
                var types = ['rapidly', 'methodically', 'brutaly', 'viciously'];
                engine.raidEngine.log(raid.id, `*${character.name}* ${types[parseInt(Math.random() * types.length)]} kills ${raid.targets[0].name}..`);
                raid.targets.splice(0, 1);
                var expGained = raid.players.length;
                raid.players.forEach(function(characterId) {
                  engine.character.addExp(characterId, expGained);
                });
                engine.raidEngine.log(raid.id, `You gain ${expGained} experience point${expGained > 1 ? 's' : ''}`);
              }
            } else {
              engine.raidEngine.log(raid.id, `${character.name} [attacks] the *air* furiously..`);
            }
          }
          break;
        case 'revive':
          var character = engine.characters.find(c => c.private == data.private);
          if (character) {
            if (character.hp !== 0) return;
            var raid = engine.raidEngine.raids.find(r => r.players.includes(character.id));
            if (character.vaccines >= character.life) {
              await engine.db.query(`UPDATE characters SET hp = ${character.life}, vaccines = ${character.vaccines - character.life} WHERE id = ${character.id}`);
              character.hp = character.life;
              engine.raidEngine.log(raid.id, `*${character.name}* [revives] himself for ${character.life} vaccines.`);
            } else {
              engine.raidEngine.log(raid.id, `*${character.name}* fails to revive himself..`);
            }
          }
          break;
      }
    });

    user.socket.on('_ping', async function(data) {
      data = data;

      var character = engine.characters.find(c => c.private == data.private);
      var pongData = {};

      if (data.query.includes('raid')) {
        pongData.raids = engine.raidEngine.raids;
        
        var raid = engine.raidEngine.raids.find(r => r.players.includes(character.id));
        if (raid) {
          pongData.raid = raid;
          pongData.players = raid.players.map(function (characterId) {
            var character = engine.characters.find(c => c.id == characterId);
            return {
              id: character.id,
              name: character.name,
              life: character.life,
              hp: character.hp,
            };
          });
          pongData.hp = character.hp;
        }
      }

      user.socket.emit('_pong', pongData);
    });
  });

  http.listen(process.env.PORT || 3000);
};

module.exports = SocketServer;