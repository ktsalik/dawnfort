var moment = require('moment');
var uniqid = require('uniqid');

var Game = function(database) {
  var _this = this;

  this.db = database;

  this.characters = [];
  this.ticks = [];
  this.db.query(`SELECT * FROM characters`).then(function(characters) {
    _this.characters = characters;
  });

  var energyInterval = 60 * 15;
  var nerveInterval = 60 * 5;
  var hungerInterval = 60 * 60;
  var hpInterval = 11;

  setInterval(function() {
    _this.characters.forEach(async function(character) {
      var tick = _this.ticks.find(t => t.id == character.id);
      if (tick) {
        tick.energy--;
        tick.nerve--;
        tick.hunger--;
        tick.hp--;
        if (tick.energy === 0 || tick.nerve === 0 || tick.hunger === 0 || tick.hp === 0) {
          var characterValues = (await _this.db.query(`
            SELECT energy, nerve, hunger, concentration,
                  _energy, _nerve, _hunger, _concentration, hp, life,
                  vaccines, coins
            FROM characters
            WHERE id = ${character.id}
          `))[0];
          var v = characterValues;
          character.life = v.life;
          // character.hp = v.hp; //FIXME: bug-hunt
          character.energy = v.energy; character._energy = v._energy;
          character.nerve = v.nerve; character._nerve = v._nerve;
          character.hunger = v.hunger; character._hunger = v._hunger;
          character.concentration = v.concentration; character._concentration = v._concentration;
          character.vaccines = v.vaccines;
          character.coins = v.coins;
          if (tick.energy === 0) {
            if (characterValues._energy < characterValues.energy) {
              await _this.db.query(`UPDATE characters SET _energy = ${characterValues._energy + 5} WHERE id = ${character.id}`);
            }
            tick.energy = energyInterval;
          }

          if (tick.nerve === 0) {
            if (characterValues._nerve < characterValues.nerve) {
              await _this.db.query(`UPDATE characters SET _nerve = ${characterValues._nerve + 1} WHERE id = ${character.id}`);
            }
            tick.nerve = nerveInterval;
          }

          if (tick.hunger === 0) {
            if (characterValues._hunger > 0) {
              await _this.db.query(`UPDATE characters SET _hunger = ${characterValues._hunger - 1}, _concentration = ${50 + (50 * ((characterValues._hunger - 1) / characterValues.hunger))} WHERE id = ${character.id}`);
            }
            tick.hunger = hungerInterval;
          }

          if (tick.hp === 0) {
            if (character.hp > 0 && characterValues.hp < characterValues.life) {
              await _this.db.query(`UPDATE characters SET hp = ${characterValues.hp + 1} WHERE id = ${character.id}`);
              character.hp = characterValues.hp + 1;
            }
            tick.hp = hpInterval;
          }
        }

        if (character.hp === 0) {
          tick.revival--;
          if (tick.revival === 0) {
            await _this.db.query(`UPDATE characters SET hp = ${character.life} WHERE id = ${character.id}`);
            character.hp = character.life;
          }
        }
      } else {
        _this.ticks.push({
          id: character.id,
          energy: energyInterval,
          nerve: nerveInterval,
          hunger: hungerInterval,
          hp: hpInterval,
          revival: 1,
        });
      }
    });
  }, 1000);

  setInterval(async function() {
    var arces = (await _this.db.query(`SELECT * FROM arces WHERE state = 1`));
    arces.forEach(async function(arce) {
      var duration = moment.duration(moment().diff(arce.sow));
      if (60 - duration.asMinutes() <= 0) {
        await _this.db.query(`UPDATE arces SET state = 2 WHERE id = ${arce.id}`);
      }
    });
  }, 3000);

  var maxGrainPrice = 1000;
  var grainPrice = parseInt(maxGrainPrice * 0.5);
  this.grainPrice = grainPrice;
  setInterval(function() {
    if (Math.random() < 0.444) {
      grainPrice -= parseInt(Math.random() * (maxGrainPrice * 0.111));
      grainPrice = grainPrice > 0 ? grainPrice : 1;
    } else {
      grainPrice += parseInt(Math.random() * (maxGrainPrice * 0.111));
      grainPrice = grainPrice <= maxGrainPrice ? grainPrice : maxGrainPrice;
    }
    _this.grainPrice = grainPrice;
  }, 11111);

  this.character = {};

  this.character.create = async function(userId) {
    var newCharacterName = (await _this.db.query(`SELECT username FROM users WHERE id = ${userId}`))[0].username;
    return new Promise(function(resolve) {
      _this.db.query(`
        INSERT INTO characters (
          user_id, name, level, life,
          strength, agility, vitality, skill, hp,
          energy, nerve, hunger, concentration,
          _energy, _nerve, _hunger, _concentration
        )
        VALUES (
          ${userId}, '${newCharacterName}', 1, 100,
          11, 11, 11, 11, 100,
          100, 9, 100, 100,
          100, 9, 100, 100
        )
      `).then(async function() {
        await _this.db.query(`INSERT INTO stats (user_id) VALUES (${userId})`);
        _this.characters = (await _this.db.query(`SELECT * FROM characters`));
        resolve();
      });
    });
  };

  this.character.addExp = async function(characterId, expAmount) {
    var character = (await _this.db.query(`SELECT * FROM characters WHERE id = ${characterId}`))[0];
    
    var getLevelExp = function (level) {
      var exp = (level * 111);
      exp += parseInt(level / 10) * 333;
      exp += parseInt(level / 50) * 9999;
      exp += parseInt(level / 100) * 999999;
      return exp;
    };

    // var currentLevelExp = getLevelExp(character.level);
    // var newExp = 0;
    // if (character.experience + expAmount >= currentLevelExp) {
    //   newExp = (character.experience + expAmount) - currentLevelExp;
    //   await _this.db.query(`UPDATE characters SET level = ${character.level + 1}, experience = ${newExp} WHERE id = ${character.id}`);
    // } else {
    //   newExp = character.experience + expAmount;
    //   await _this.db.query(`UPDATE characters SET experience = ${newExp} WHERE id = ${character.id}`);
    // }

    await _this.db.query(`UPDATE characters SET experience = ${character.experience + expAmount} WHERE id = ${character.id}`);
  };

  this.character.scavenge = async function(userId, area) {
    var character = (await _this.db.query(`SELECT * FROM characters WHERE user_id = ${userId}`))[0];

    if (character._nerve < area) {
      return {
        status: 'error',
        message: 'You do not have enough nerve available',
      };
    }

    var stats = (await _this.db.query(`SELECT * FROM stats WHERE user_id = ${userId}`))[0];
    
    var successPerc = 1;
    if (area - 1 > 0) {
      successPerc = (stats.scavenges + 1) / (area * 1000);
    }
    if (area - 5 > 0) {
      successPerc = (stats.scavenges + 1) / (area * 10000);
    }

    var successMessages = [
      `You were able to avoid stepping on the broken glass and stayed silent, successfully exploring the Restaurant.`,
      `You manage to cut the lock off the warehouse door and rummage through the boxes without making a sound..`,
      `You sure are glad your childhood Scout group took you camping and taught you how to use a Compass. You manage to navigate around without any issues..`,
      `You manage to climb your way up to the attic without alerting the zombies below..`,
      `You manage to avoid getting poisoned by toxic fumes and successfully loot the Arcade.`,
      `You scavenged the area successfully..`,
      `You scavenged the area successfully..`,
      `You scavenged the area successfully..`,
      `You scavenged the area successfully..`,
    ];
    
    var failMessages = [
      `You were able to scavenge the restaurant, but you stepped on a broken coffeepot and alerted the zombiefied waitress. You manage to escape without getting eaten, but you leave empty-handed.`,
      `When you cut the lock, the piece of metal slipped through your fingers and hit the concrete with a loud clatter. You managed to escape before the moaning zombies found you.`,
      `You take a wrong turn and end up lost in the food court. You find nothing useful and look longingly at the place you used to get cinnamon buns with your friends.`,
      `You get inside the gate of Greek Row, but then you trip over an empty keg. The clatter alerts every ZomBro around, and you manage to run before they can get you.`,
      `You begin poking through the broken games, looking for loot, when you trip over a power cord and fall into the arms of...a zombie. Thankfully, you manage to wiggle away before he can bite you.`,
      `You alert every ZomBro around, you manage to run before they can get you.`,
      `You alert every ZomBro around, you manage to run before they can get you.`,
      `You alert every ZomBro around, you manage to run before they can get you.`,
      `You alert every ZomBro around, you manage to run before they can get you.`,
    ];
    
    var scavenged = successPerc > Math.random();
    if (scavenged) {
      var result;
      var foundVaccines = Math.random() > 0.333;
      var vaccinesFound = 0;
      if (foundVaccines) {
        vaccinesFound = parseInt((area * 100) * Math.random());
        result = {
          status: 'ok',
          message: successMessages[area - 1] + ` You find ${vaccinesFound} vaccines and gain some experience.`,
        };
      } else {
        result = {
          status: 'info',
          message: successMessages[area - 1] + ` You find nothing but gain some experience.`,
        };
      }
      var expGain = 1 + ((area * 11) * Math.random());
      _this.character.addExp(character.id, expGain);
      (await _this.db.query(`UPDATE stats SET scavenges = ${stats.scavenges + 1} WHERE user_id = ${userId}`));
      (await _this.db.query(`UPDATE characters SET _nerve = ${character._nerve - area}, vaccines = ${character.vaccines + vaccinesFound} WHERE id = ${character.id}`));
      return result;
    } else {
      (await _this.db.query(`UPDATE characters SET _nerve = ${character._nerve - area} WHERE id = ${character.id}`));
      return {
        status: 'error',
        message: failMessages[area - 1],
      };
    }
  };

  this.raidEngine = {
    raids: [],
    loadRaids: function() {
      _this.db.query(`SELECT * FROM raids`).then(async function (raids) {
        for (var i = 0; i < raids.length; i++) {
          var raid = raids[i];
          // players
          raid.players = [];
          var players = (await _this.db.query(`SELECT character_id FROM raids JOIN raid_characters WHERE id = raid_id AND raid_id = ${raid.id} ORDER BY id`));
          raid.players = players.map(c => c.character_id);
          if (raid.players.length === 0) {
            raids.splice(i--, 1);
            _this.db.query(`DELETE FROM raids WHERE id = ${raid.id}`);
          }

          // target
          raid.targets = [];
          if (_this.raidEngine.raids.findIndex(r => r.id == raid.id) == -1) {
            _this.raidEngine.raids.push(raid);
          } else {
            localRaid = _this.raidEngine.raids.find(r => r.id == raid.id);
            localRaid.players = raid.players;
          }
        }
        
        _this.raidEngine.raids.forEach(function (raid) {
          if (!raids.map(r => r.id).includes(raid.id)) {
            _this.raidEngine.raids.splice(_this.raidEngine.raids.findIndex(r => r.id == raid.id), 1);
          }
        });
      });
    },
    log: async function(raidId, log) {
      return await _this.db.query(`INSERT INTO raid_logs (raid_id, data) VALUES (${raidId}, '${log}')`);
    },
  };
  this.raidEngine.loadRaids();

  setInterval(async function raidEntroy() {
    var raids = _this.raidEngine.raids;

    for (var i = 0; i < raids.length; i++) {
      var raid = raids[i];

      var alivePlayers = raid.players.filter(function (characterId) {
        return _this.characters.find(c => c.id == characterId).hp > 0;
      });

      if (alivePlayers.length && raid.targets.length) {
        for (var k = 0; k < raid.targets.length; k++) {
          var zom = raid.targets[k];
          var playerToAttack = alivePlayers[parseInt(Math.random() * alivePlayers.length)];
          var character = _this.characters.find(c => c.id == playerToAttack);
          var damage = parseInt(zom.stats.ad - ((character.strength * 0.025) + (character.agility * 0.075)));
          if ((character.hp - damage) > 0) {
            await _this.db.query(`UPDATE characters SET hp = ${character.hp - damage} WHERE id = ${character.id}`);
            character.hp -= damage;
            await _this.raidEngine.log(raid.id, `*${zom.name}* [attacks] (${character.name}) doing ${damage} damage.`);
          } else {
            character.hp = 0;
            await _this.db.query(`UPDATE characters SET hp = 0 WHERE id = ${character.id}`);
            _this.ticks.find(t => t.id == character.id).revival = parseInt((60 * 5) + (60 * 5 * Math.random()));
            await _this.raidEngine.log(raid.id, `#${zom.name}# [attacks] (${character.name}) doing ${damage} damage.`);
            await _this.raidEngine.log(raid.id, `*${character.name}* lies on the floor unable to take any action..`);
          }
        }
      }

      if (raid.targets.length < raid.players.length) {
        var partyLifes = raid.players.map(function(characterId) {
          var character = _this.characters.find(c => c.id == characterId);
          return character.life;
        });
        partyLifes = partyLifes.sort(function(a, b) { if (a > b) return 1; else return -1; });
        var partyAds = raid.players.map(function (characterId) {
          var character = _this.characters.find(c => c.id == characterId);
          return character.strength * 0.7;
        });
        partyAds = partyAds.sort(function (a, b) { if (a > b) return 1; else return -1; });
        var zomLife = partyLifes[Math.ceil(partyLifes.length / 2) - 1];
        // var zomAd = Math.min(...partyLifes) * 0.95;
        var zomAd = partyAds[Math.ceil(partyAds.length / 2) - 1];
        var zom = {
          name: 'zomBro' + uniqid().split('').reverse().slice(0, 3).join(''),
          stats: {
            life: zomLife,
            hp: zomLife,
            ad: zomAd,
            def: 0,
          },
        };
        raid.targets.push(zom);
        _this.raidEngine.log(raid.id, `${zom.name} appears with ${zom.stats.hp} life!`);
      }
    }
  }, 7000);

  setInterval(function() {
    _this.raidEngine.raids.forEach(async function(raid) {
      raid.logs = (await _this.db.query(`SELECT data FROM raid_logs WHERE raid_id = ${raid.id} ORDER BY id DESC LIMIT 30`)).map(row => row.data);
    });
  }, 1000);
  
  this.raidEngine.createRaid = async function(characterId) {
    var alreadyInRaid = _this.raidEngine.raids.find(r => r.players.includes(characterId));
    if (alreadyInRaid) {
      return;
    }
    var raidId = (await _this.db.query(`INSERT INTO raids () VALUES ()`)).insertId;
    await _this.db.query(`INSERT INTO raid_characters (raid_id, character_id) VALUES (${raidId}, ${characterId})`);
    _this.raidEngine.loadRaids();
    await this.log(raidId, `${_this.characters.find(c => c.id == characterId).name} started a Raid Party`);
  };

  this.raidEngine.joinRaid = async function(raidId, characterId) {
    await _this.db.query(`INSERT INTO raid_characters (raid_id, character_id) VALUES (${raidId}, ${characterId})`);
    _this.raidEngine.loadRaids();
    await this.log(raidId, `${_this.characters.find(c => c.id == characterId).name} joined the Party`);
  };

  this.raidEngine.leaveRaid = async function (characterId) {
    var raidId = (await _this.db.query(`SELECT raid_id FROM raid_characters WHERE character_id = ${characterId}`))[0].raid_id;
    await _this.db.query(`DELETE FROM raid_characters WHERE character_id = ${characterId}`);
    _this.raidEngine.loadRaids();
    await this.log(raidId, `${_this.characters.find(c => c.id == characterId).name} left the Party`);
  };
};

module.exports = Game;